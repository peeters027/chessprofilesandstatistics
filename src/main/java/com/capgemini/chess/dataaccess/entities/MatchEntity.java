package com.capgemini.chess.dataaccess.entities;

import com.capgemini.chess.enums.MatchResult;

public class MatchEntity {

	private long id;
	private long hostId;
	private long guestId;
	private MatchResult matchResult;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getHostId() {
		return hostId;
	}

	public void setHostId(long hostId) {
		this.hostId = hostId;
	}

	public long getGuestId() {
		return guestId;
	}

	public void setGuestId(long guestId) {
		this.guestId = guestId;
	}

	public MatchResult getMatchResult() {
		return matchResult;
	}

	public void setMatchResult(MatchResult matchResult) {
		this.matchResult = matchResult;
	}
}
