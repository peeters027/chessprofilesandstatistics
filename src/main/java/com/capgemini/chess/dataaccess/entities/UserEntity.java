package com.capgemini.chess.dataaccess.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToOne;

@Entity
public class UserEntity {

	@Id
	@GeneratedValue
	private long id;
	@Column(nullable = false)
	private String login;
	@Column(nullable = false)
	private String password;
	private String name;
	private String surname;
	private String email;
	@Lob
	private String aboutMe;
	@Lob
	private String lifeMotto;
	@OneToOne
	private UserStatisticsEntity statistics;
	
	public UserEntity()
	{}

	public UserStatisticsEntity getStatistics() {
		return statistics;
	}

	public void setStatistics(UserStatisticsEntity statistics) {
		this.statistics = statistics;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAboutMe() {
		return aboutMe;
	}

	public void setAboutMe(String aboutMe) {
		this.aboutMe = aboutMe;
	}

	public String getLifeMotto() {
		return lifeMotto;
	}

	public void setLifeMotto(String lifeMotto) {
		this.lifeMotto = lifeMotto;
	}
	
	private UserEntity(Builder builder) {

		this.id = builder.id;
		this.login = builder.login;
		this.password = builder.password;
		this.name = builder.name;
		this.surname = builder.surname;
		this.email = builder.email;
		this.aboutMe = builder.aboutMe;
		this.lifeMotto = builder.lifeMotto;
		this.statistics = builder.statistics;
	}

	public static class Builder {
		private long id;
		private String login;
		private String password;
		private String name;
		private String surname;
		private String email;
		private String aboutMe;
		private String lifeMotto;
		private UserStatisticsEntity statistics;

		public Builder id(long id) {
			this.id = id;
			return this;
		}

		public Builder login(String login) {
			this.login = login;
			return this;
		}

		public Builder password(String password) {
			this.password = password;
			return this;
		}

		public Builder name(String name) {
			this.name = name;
			return this;
		}

		public Builder surname(String surname) {
			this.surname = surname;
			return this;
		}

		public Builder email(String email) {
			this.email = email;
			return this;
		}

		public Builder aboutMe(String aboutMe) {
			this.aboutMe = aboutMe;
			return this;
		}

		public Builder lifeMotto(String lifeMotto) {
			this.lifeMotto = lifeMotto;
			return this;
		}

		public Builder statistics(UserStatisticsEntity statistics) {
			this.statistics = statistics;
			return this;
		}

		public UserEntity build() {
			return new UserEntity(this);
		}
	}

}
