package com.capgemini.chess.dataaccess.dao.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.capgemini.chess.dataaccess.entities.MatchEntity;
import com.capgemini.chess.service.access.MatchDao;
import com.capgemini.chess.service.mapper.MatchMapper;
import com.capgemini.chess.service.to.MatchTO;

@Repository
public class MapMatchDao implements MatchDao {

	private static final Map<Long, MatchEntity> matches = new HashMap<>();

	public static Map<Long, MatchEntity> getMatches() {
		return matches;
	}

	@Override
	public MatchTO saveMatch(MatchTO matchTo) {
		MatchEntity matchEntity = MatchMapper.map(matchTo);
		long id = generateId();
		matchTo.setId(id);
		matches.put(id, matchEntity);

		return MatchMapper.map(matchEntity);
	}

	private Long generateId() {
		return matches.keySet().stream().max((i1, i2) -> i1.compareTo(i2)).orElse(0L) + 1;
	}

}
