package com.capgemini.chess.dataaccess.dao.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.capgemini.chess.dataaccess.entities.UserEntity;
import com.capgemini.chess.dataaccess.entities.UserStatisticsEntity;
import com.capgemini.chess.enums.Level;
import com.capgemini.chess.service.access.UserDao;
import com.capgemini.chess.service.mapper.UserProfileMapper;
import com.capgemini.chess.service.mapper.UserStatisticsMapper;
import com.capgemini.chess.service.to.UpdateProfileTO;
import com.capgemini.chess.service.to.UserProfileTO;
import com.capgemini.chess.service.to.UserStatsTO;

@Repository
public class MapUserDao implements UserDao {

	private static Map<Long, UserEntity> users = new HashMap<>();
	
	public MapUserDao()
	{
		setDataBase();
	}

	public static Map<Long, UserEntity> getUsers() {
		return users;
	}

	@Override
	public UserProfileTO save(UserProfileTO userTo) {
		Long id = generateId();
		UserEntity userEntity = UserProfileMapper.map(userTo);
		//UserStatsTO userStatsTo = userTo.getStatistics();
		//if (userStatsTo == null) {
		UserStatsTO	userStatsTo = getDafaultStats(id);
		//}
		UserStatisticsEntity userStatisticsEntity = UserStatisticsMapper.map(userStatsTo);
		userEntity.setStatistics(userStatisticsEntity);
		userTo.setId(id);
		users.put(id, userEntity);
		return userTo;
	}

	private Long generateId() {
		return users.keySet().stream().max((i1, i2) -> i1.compareTo(i2)).orElse(0L) + 1;
	}

	@Override
	public UserProfileTO update(UpdateProfileTO to) {
		long id = to.getId();
		UserProfileTO userTo = findById(id);
		UserEntity userEntity = UserProfileMapper.map(userTo);
		userEntity = UserProfileMapper.update(userEntity, to);
		users.put(to.getId(), userEntity);
		return UserProfileMapper.map(userEntity);
	}

	@Override
	public UserStatsTO updateUserStats(UserStatsTO userStatsTo) {
		long id = userStatsTo.getId();
		UserProfileTO userTo = findById(id);
		UserStatisticsEntity userStatisticsEntity = UserStatisticsMapper.map(userStatsTo);
		UserEntity userEntity = UserProfileMapper.map(userTo);
		userEntity.setStatistics(userStatisticsEntity);
		users.put(userStatsTo.getId(), userEntity);
		return userStatsTo;
	}

	@Override
	public UserProfileTO findById(long id) {
		UserEntity user = users.get(id);
		return UserProfileMapper.map(user);
	}

	@Override
	public UserStatsTO findStatsById(long id) {
		UserStatisticsEntity user = users.get(id).getStatistics();
		return UserStatisticsMapper.map(user);
	}

	@Override
	public UserProfileTO findByEmail(String email) {
		UserEntity user = users.values().stream().filter(u -> u.getEmail().equals(email)).findFirst().orElse(null);
		return UserProfileMapper.map(user);
	}

	UserStatsTO getDafaultStats(long id) {
		UserStatsTO defaultStats = new UserStatsTO.Builder().id(id).gamesPlayed(0).won(0).lost(0).draw(0)
				.rankingPoints(100).level(Level.NEWBIE).build();
		return defaultStats;
	}

	@Override
	public List<UserStatsTO> readRanking() {
		List<UserStatisticsEntity> listOfUsersStatistics = new ArrayList<UserStatisticsEntity>();
		List<UserEntity> listOfUsers = new ArrayList<UserEntity>();
		listOfUsers.addAll(users.values());
		for (int i = 0; i < listOfUsers.size(); i++) {
			listOfUsersStatistics.add(listOfUsers.get(i).getStatistics());
		}
		Collections.sort(listOfUsersStatistics, new Comparator<UserStatisticsEntity>() {
			@Override
			public int compare(UserStatisticsEntity o1, UserStatisticsEntity o2) {
				return o2.getRankingPoints() - o1.getRankingPoints();
			}
		});
		return UserStatisticsMapper.mapStats2TOs(listOfUsersStatistics);
	}
	
	
	
	public void setDataBase()
	{
		
		UserStatsTO firstUserStats = new UserStatsTO.Builder().id(1).gamesPlayed(100)
				.won(50)
				.lost(50)
				.draw(0)
				.rankingPoints(1000)
				.level(Level.BEGINNER)
				.build();
		
		UserProfileTO firstUser = new UserProfileTO.Builder().id(1)
				.login("Lion")
				.email("piotr@gmail.com")
				.password("123123123")
				.name("Piotr")
				.surname("Szmolke")
				.aboutMe("Happy")
				.lifeMotto("Dopóki walczysz jesteś zwycięzcą")
				.statistics(firstUserStats)
				.build();
		
		UserStatsTO secondUserStats = new UserStatsTO.Builder().id(2).gamesPlayed(100)
				.won(50)
				.lost(50)
				.draw(0)
				.rankingPoints(2000)
				.level(Level.BEGINNER)
				.build();
		
		UserProfileTO secondUser = new UserProfileTO.Builder().id(2)
				.login("SpiderMan")
				.email("marek@gmail.com")
				.password("qazwsx3edc")
				.name("Marek")
				.surname("Nowak")
				.aboutMe("...")
				.lifeMotto("Always look on the bright side of life")
				.statistics(secondUserStats)
				.build();
		
		UserStatsTO thirdUserStats = new UserStatsTO.Builder().id(3).gamesPlayed(100)
				.won(50)
				.lost(50)
				.draw(0)
				.rankingPoints(500)
				.level(Level.BEGINNER)
				.build();
		
		UserProfileTO thirdUser = new UserProfileTO.Builder().id(3)
				.login("crazyGirl")
				.email("ania@gmail.com")
				.password("53423424432")
				.name("Anna")
				.surname("Kowalska")
				.aboutMe("I love chess!")
				.lifeMotto("Just do it!")
				.statistics(thirdUserStats)
				.build();
		
		long firstId = 1;
		long secondId = 2;
		long thirdId = 3;

		UserEntity firstUserEntity = UserProfileMapper.map(firstUser);
		firstUserEntity.setStatistics(UserStatisticsMapper.map(firstUser.getStatistics()));

		UserEntity secondUserEntity = UserProfileMapper.map(secondUser);
		secondUserEntity.setStatistics(UserStatisticsMapper.map(secondUser.getStatistics()));

		UserEntity thirdUserEntity = UserProfileMapper.map(thirdUser);
		thirdUserEntity.setStatistics(UserStatisticsMapper.map(thirdUser.getStatistics()));

		users.put(firstId, firstUserEntity);
		users.put(secondId, secondUserEntity);
		users.put(thirdId, thirdUserEntity);
	}
}
