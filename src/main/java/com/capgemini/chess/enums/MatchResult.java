package com.capgemini.chess.enums;

public enum MatchResult {

	HostWon, GuestWon, Draw

}
