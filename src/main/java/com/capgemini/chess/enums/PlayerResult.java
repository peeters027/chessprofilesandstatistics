package com.capgemini.chess.enums;

public enum PlayerResult {

	Won, Lost, Draw
}
