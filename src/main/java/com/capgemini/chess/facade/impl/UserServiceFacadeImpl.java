package com.capgemini.chess.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.chess.exception.UserValidationException;
import com.capgemini.chess.facade.UserServiceFacade;
import com.capgemini.chess.service.MatchRegisterService;
import com.capgemini.chess.service.RankingService;
import com.capgemini.chess.service.UserUpdateService;
import com.capgemini.chess.service.to.MatchTO;
import com.capgemini.chess.service.to.RankingTO;
import com.capgemini.chess.service.to.UpdateProfileTO;
import com.capgemini.chess.service.to.UserProfileTO;

@Service
public class UserServiceFacadeImpl implements UserServiceFacade {

	@Autowired
	private UserUpdateService updateService;
	
	@Autowired
	private MatchRegisterService matchRegisterService;
	
	@Autowired
	private RankingService rankingService;

	@Override
	public UserProfileTO updateProfile(UpdateProfileTO to) throws UserValidationException {
		return updateService.updateProfile(to);
	}

	@Override
	public RankingTO getRanking(long id) throws UserValidationException {
		return rankingService.getRanking(id);
	}

	@Override
	public MatchTO registerNewMatch(MatchTO matchTo) throws UserValidationException {
		return matchRegisterService.registerNewMatch(matchTo);
	}
}
