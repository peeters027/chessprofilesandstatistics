package com.capgemini.chess.facade;

import com.capgemini.chess.exception.UserValidationException;
import com.capgemini.chess.service.to.MatchTO;
import com.capgemini.chess.service.to.RankingTO;
import com.capgemini.chess.service.to.UpdateProfileTO;
import com.capgemini.chess.service.to.UserProfileTO;

public interface UserServiceFacade {
	UserProfileTO updateProfile(UpdateProfileTO to) throws UserValidationException;
	RankingTO getRanking(long id) throws UserValidationException;
	MatchTO registerNewMatch(MatchTO matchTo) throws UserValidationException;
}
