package com.capgemini.chess.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.capgemini.chess.exception.UserValidationException;
import com.capgemini.chess.service.MatchRegisterService;
import com.capgemini.chess.service.RankingService;
import com.capgemini.chess.service.UserUpdateService;
import com.capgemini.chess.service.access.UserDao;
import com.capgemini.chess.service.to.MatchTO;
import com.capgemini.chess.service.to.RankingTO;
import com.capgemini.chess.service.to.UpdateProfileTO;
import com.capgemini.chess.service.to.UserProfileTO;

@Controller
@ResponseBody
public class RestService {

	@Autowired
	private RankingService rankingService;

	@Autowired
	private UserUpdateService userUpdateService;

	@Autowired
	private MatchRegisterService matchRegisterService;

	@Autowired
	private UserDao userDao;

	/**
	 * Get full ranking of users and ranking position for user with specified ID
	 * @param id the id of the user
	 * @return full ranking and ranking position
	 * @throws UserValidationException
	 */
	@RequestMapping(value = "/findStats/{id}")
	public RankingTO findRankById(@PathVariable("id") long id) throws UserValidationException {
		return rankingService.getRanking(id);
	}
	
	/**
	 * Get user information by ID
	 * @param id the id of the user
	 * @return full info about user
	 * @throws UserValidationException
	 */

	@RequestMapping(value = "/findUser/{id}")
	public UserProfileTO findUser(@PathVariable("id") long id) throws UserValidationException {
		return userDao.findById(id);
	}
	
	/**
	 * Post method for updating profile
	 * @param updateProfileTo data about user that is going to be updated 
	 * @return full info about user
	 * @throws UserValidationException
	 */

	@RequestMapping(value = "/updateUser", method = RequestMethod.POST)
	public UserProfileTO updateProfile(@RequestBody UpdateProfileTO updateProfileTo) throws UserValidationException {
		return userUpdateService.updateProfile(updateProfileTo);
	}

	/**
	 * Post method for registering new match and updating statistics both players based on match result
	 * @param matchTo the match that is going to be registered
	 * @return saved match
	 * @throws UserValidationException
	 */
	
	@RequestMapping(value = "/registerNewMatch", method = RequestMethod.POST)
	public MatchTO registerNewMatch(@RequestBody MatchTO matchTo) throws UserValidationException {
		return matchRegisterService.registerNewMatch(matchTo);
	}
}
