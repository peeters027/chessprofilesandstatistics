package com.capgemini.chess.resty;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.capgemini.chess.exception.UserValidationException;
import com.capgemini.chess.service.RankingService;
import com.capgemini.chess.service.access.UserDao;
import com.capgemini.chess.service.to.RankingTO;
import com.capgemini.chess.service.to.UserProfileTO;

@Controller
@ResponseBody
public class RankingRestService {
	
	/*@Autowired
	private RankingService rankingService;
	
	@RequestMapping(value="/findStats/{id}")
	public RankingTO findRankById(@PathVariable("id") long id) throws UserValidationException
	{
		return rankingService.getRanking(id);
	}*/

	
	@Autowired
	private UserDao userDao;
	
	@RequestMapping(value="/findStats/{id}")
	public UserProfileTO findRankById(@PathVariable("id") long id) throws UserValidationException
	{
		return userDao.findById(id);
	}
}
