package com.capgemini.chess.service;

import com.capgemini.chess.exception.UserValidationException;
import com.capgemini.chess.service.to.RankingTO;

public interface RankingService {

	public RankingTO getRanking(long id) throws UserValidationException;

}
