package com.capgemini.chess.service.access;

import com.capgemini.chess.service.to.MatchTO;

public interface MatchDao {
	
	public MatchTO saveMatch(MatchTO matchTo);

}
