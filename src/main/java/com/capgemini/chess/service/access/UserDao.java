package com.capgemini.chess.service.access;

import java.util.List;

import com.capgemini.chess.service.to.UpdateProfileTO;
import com.capgemini.chess.service.to.UserProfileTO;
import com.capgemini.chess.service.to.UserStatsTO;

public interface UserDao {
	
	public UserProfileTO findById(long id);
	
	public UserStatsTO findStatsById(long id);
	
	public UserProfileTO findByEmail(String email);
	
	public UserProfileTO update(UpdateProfileTO to);

	public List<UserStatsTO> readRanking();
	
	public UserStatsTO updateUserStats(UserStatsTO userStatsTo);
	
	public UserProfileTO save(UserProfileTO userTo);
	
}
