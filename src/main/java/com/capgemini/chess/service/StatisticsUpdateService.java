package com.capgemini.chess.service;

import com.capgemini.chess.exception.UserValidationException;
import com.capgemini.chess.service.to.MatchTO;

public interface StatisticsUpdateService {

	MatchTO updateStatistics(MatchTO matchTO) throws UserValidationException;

}
