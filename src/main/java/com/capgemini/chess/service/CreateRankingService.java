package com.capgemini.chess.service;

import com.capgemini.chess.service.to.RankingTO;

public interface CreateRankingService {

	RankingTO create(long id);

}
