package com.capgemini.chess.service.mapper;

import com.capgemini.chess.dataaccess.entities.MatchEntity;
import com.capgemini.chess.service.to.MatchTO;

public class MatchMapper {

	public static MatchEntity map(MatchTO matchTo) {
		MatchEntity matchEntity = new MatchEntity();
		matchEntity.setId(matchTo.getId());
		matchEntity.setHostId(matchTo.getHostId());
		matchEntity.setGuestId(matchTo.getGuestId());
		matchEntity.setMatchResult(matchTo.getResult());
		return matchEntity;
	}

	public static MatchTO map(MatchEntity matchEntity) {
		MatchTO matchTo = new MatchTO();
		matchTo.setId(matchEntity.getId());
		matchTo.setHostId(matchEntity.getHostId());
		matchTo.setGuestId(matchEntity.getGuestId());
		matchTo.setResult(matchEntity.getMatchResult());
		return matchTo;
	}

}
