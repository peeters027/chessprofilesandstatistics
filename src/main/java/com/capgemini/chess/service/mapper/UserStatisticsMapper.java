package com.capgemini.chess.service.mapper;

import java.util.List;
import java.util.stream.Collectors;

import com.capgemini.chess.dataaccess.entities.UserStatisticsEntity;
import com.capgemini.chess.service.to.UserStatsTO;

public class UserStatisticsMapper {

	public static UserStatsTO map(UserStatisticsEntity userEntity) {
		if (userEntity != null) {
			UserStatsTO userTO = new UserStatsTO();
			userTO.setId(userEntity.getId());
			userTO.setGamesPlayed(userEntity.getGamesPlayed());
			userTO.setWon(userEntity.getWon());
			userTO.setLost(userEntity.getLost());
			userTO.setDraw(userEntity.getDraw());
			userTO.setRankingPoints(userEntity.getRankingPoints());
			userTO.setLevel(userEntity.getLevel());

			return userTO;
		}
		return null;
	}

	public static UserStatisticsEntity map(UserStatsTO userStatsTo) {
		if (userStatsTo != null) {
			UserStatisticsEntity userStatisticsEntity = new UserStatisticsEntity();
			userStatisticsEntity.setId(userStatsTo.getId());
			userStatisticsEntity.setGamesPlayed(userStatsTo.getGamesPlayed());
			userStatisticsEntity.setWon(userStatsTo.getWon());
			userStatisticsEntity.setLost(userStatsTo.getLost());
			userStatisticsEntity.setDraw(userStatsTo.getDraw());
			userStatisticsEntity.setRankingPoints(userStatsTo.getRankingPoints());
			userStatisticsEntity.setLevel(userStatsTo.getLevel());
			return userStatisticsEntity;
		}
		return null;
	}

	public static UserStatisticsEntity update(UserStatisticsEntity userEntity, UserStatsTO userTO) {
		if (userTO != null && userEntity != null) {
			userEntity.setGamesPlayed(userTO.getGamesPlayed());
			userEntity.setWon(userTO.getWon());
			userEntity.setLost(userTO.getLost());
			userEntity.setDraw(userTO.getDraw());
			userEntity.setRankingPoints(userTO.getRankingPoints());
			userEntity.setLevel(userTO.getLevel());
		}
		return userEntity;
	}

	public static List<UserStatsTO> mapStats2TOs(List<UserStatisticsEntity> userStatisticsEntities) {
		return userStatisticsEntities.stream().map(UserStatisticsMapper::map).collect(Collectors.toList());
	}

	public static List<UserStatisticsEntity> mapStats2Entities(List<UserStatsTO> userStatsTOs) {
		return userStatsTOs.stream().map(UserStatisticsMapper::map).collect(Collectors.toList());
	}
}
