package com.capgemini.chess.service.to;

import com.capgemini.chess.enums.Level;

public class UserStatsTO {

	private long id;
	private int gamesPlayed;
	private int won;
	private int lost;
	private int draw;
	private int rankingPoints;
	private Level level;

	public UserStatsTO() {

	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getGamesPlayed() {
		return gamesPlayed;
	}

	public void setGamesPlayed(int gamesPlayed) {
		this.gamesPlayed = gamesPlayed;
	}

	public int getWon() {
		return won;
	}

	public void setWon(int won) {
		this.won = won;
	}

	public int getLost() {
		return lost;
	}

	public void setLost(int lost) {
		this.lost = lost;
	}

	public int getDraw() {
		return draw;
	}

	public void setDraw(int draw) {
		this.draw = draw;
	}

	public int getRankingPoints() {
		return rankingPoints;
	}

	public void setRankingPoints(int rankingPoints) {
		this.rankingPoints = rankingPoints;
	}

	public Level getLevel() {
		return level;
	}

	public void setLevel(Level level) {
		this.level = level;
	}

	private UserStatsTO(Builder builder) {
		this.id = builder.id;
		this.gamesPlayed = builder.gamesPlayed;
		this.won = builder.won;
		this.lost = builder.lost;
		this.draw = builder.draw;
		this.rankingPoints = builder.rankingPoints;
		this.level = builder.level;
	}

	public static class Builder {
		private long id;
		private int gamesPlayed;
		private int won;
		private int lost;
		private int draw;
		private int rankingPoints;
		private Level level;

		public Builder id(long id) {
			this.id = id;
			return this;
		}

		public Builder gamesPlayed(int gamesPlayed) {
			this.gamesPlayed = gamesPlayed;
			return this;
		}

		public Builder won(int won) {
			this.won = won;
			return this;
		}

		public Builder lost(int lost) {
			this.lost = lost;
			return this;
		}

		public Builder draw(int draw) {
			this.draw = draw;
			return this;
		}

		public Builder rankingPoints(int rankingPoints) {
			this.rankingPoints = rankingPoints;
			return this;
		}

		public Builder level(Level level) {
			this.level = level;
			return this;
		}

		public UserStatsTO build() {
			return new UserStatsTO(this);
		}

	}
}
