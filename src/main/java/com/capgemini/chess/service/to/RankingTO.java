package com.capgemini.chess.service.to;

import java.util.List;

public class RankingTO {
	
	private List<UserStatsTO> ranking;
	int rankingPlace;
	
	public List<UserStatsTO> getRanking() {
		return ranking;
	}

	public void setRanking(List<UserStatsTO> ranking) {
		this.ranking = ranking;
	}
	public int getRankingPlace() {
		return rankingPlace;
	}
	public void setRankingPlace(int rankingPlace) {
		this.rankingPlace = rankingPlace;
	}
	
	

}
