package com.capgemini.chess.service;

import com.capgemini.chess.exception.UserValidationException;
import com.capgemini.chess.service.to.UpdateProfileTO;
import com.capgemini.chess.service.to.UserProfileTO;

public interface UserUpdateService {

	public UserProfileTO updateProfile(UpdateProfileTO to) throws UserValidationException;
}
