package com.capgemini.chess.service;

import java.util.List;

import com.capgemini.chess.exception.UserValidationException;
import com.capgemini.chess.service.to.UserStatsTO;

public interface ReadService {

	List<UserStatsTO> readRanking();

	UserStatsTO getUserStats(long id) throws UserValidationException;

}
