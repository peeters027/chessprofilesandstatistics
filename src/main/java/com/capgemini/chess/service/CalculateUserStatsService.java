package com.capgemini.chess.service;

import com.capgemini.chess.enums.PlayerResult;
import com.capgemini.chess.service.to.UserStatsTO;

public interface CalculateUserStatsService {

	UserStatsTO calculateStats(UserStatsTO userStats, PlayerResult result);

}
