package com.capgemini.chess.service;

import com.capgemini.chess.exception.UserValidationException;
import com.capgemini.chess.service.to.MatchTO;

public interface MatchRegisterService {

	MatchTO registerNewMatch(MatchTO matchTo)  throws UserValidationException;

}
