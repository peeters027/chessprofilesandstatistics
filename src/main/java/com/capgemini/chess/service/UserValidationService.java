package com.capgemini.chess.service;

import com.capgemini.chess.exception.UserValidationException;
import com.capgemini.chess.service.to.UpdateProfileTO;

public interface UserValidationService {

	public void validate(UpdateProfileTO to) throws UserValidationException;

	public void validate(long id) throws UserValidationException;

}
