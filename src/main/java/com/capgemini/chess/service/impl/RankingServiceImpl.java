package com.capgemini.chess.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.chess.exception.UserValidationException;
import com.capgemini.chess.service.CreateRankingService;
import com.capgemini.chess.service.RankingService;
import com.capgemini.chess.service.UserValidationService;
import com.capgemini.chess.service.to.RankingTO;

@Service
public class RankingServiceImpl implements RankingService {

	@Autowired
	private UserValidationService validationService;
	
	@Autowired 
	private CreateRankingService createRankingService;
	
	@Override
	public RankingTO getRanking(long id) throws UserValidationException {
		validationService.validate(id);
		return createRankingService.create(id);	
	}
}
