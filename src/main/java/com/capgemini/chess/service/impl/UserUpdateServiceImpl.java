package com.capgemini.chess.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.chess.exception.UserValidationException;
import com.capgemini.chess.service.UserUpdateService;
import com.capgemini.chess.service.UserValidationService;
import com.capgemini.chess.service.access.UserDao;
import com.capgemini.chess.service.to.UpdateProfileTO;
import com.capgemini.chess.service.to.UserProfileTO;

@Service
public class UserUpdateServiceImpl implements UserUpdateService {

	@Autowired
	private UserValidationService validationService;

	@Autowired
	private UserDao userDao;

	@Override
	public UserProfileTO updateProfile(UpdateProfileTO to) throws UserValidationException {
		validationService.validate(to);
		return userDao.update(to);
	}
}
