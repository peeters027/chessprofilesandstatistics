package com.capgemini.chess.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.chess.exception.UserValidationException;
import com.capgemini.chess.service.UserValidationService;
import com.capgemini.chess.service.access.UserDao;
import com.capgemini.chess.service.to.UpdateProfileTO;
import com.capgemini.chess.service.to.UserProfileTO;

@Service
public class UserValidationServiceImpl implements UserValidationService {
	
	@Autowired
	private UserDao userDao;
	
	@Override
	public void validate(long id) throws UserValidationException {
		validateIfUserExists(id);
	}

	@Override
	public void validate(UpdateProfileTO to) throws UserValidationException {
		validateIfUserExists(to.getId());
		validateEmail(to);
		validatePassword(to);
	}
	
	private void validateIfUserExists(long id) throws UserValidationException {
		UserProfileTO foundById = userDao.findById(id);
		if (foundById == null) {
			throw new UserValidationException("User does not exist");
		}
	}
	
	private void validateEmail(UpdateProfileTO to) throws UserValidationException {
		UserProfileTO foundByEmail = userDao.findByEmail(to.getEmail());
		if (foundByEmail != null && foundByEmail.getId() != to.getId()) {
			throw new UserValidationException("User with given email already exists");
		}
		
	}

	private void validatePassword(UpdateProfileTO to) throws UserValidationException {
		if (to.getPassword() != null && to.getPassword().length() < 8) {
			throw new UserValidationException("Password should be at least 8 characters long");
		}
	}
}
