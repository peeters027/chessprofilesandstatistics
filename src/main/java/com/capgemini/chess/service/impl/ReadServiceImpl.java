package com.capgemini.chess.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.chess.exception.UserValidationException;
import com.capgemini.chess.service.ReadService;
import com.capgemini.chess.service.access.UserDao;
import com.capgemini.chess.service.to.UserStatsTO;

@Service
public class ReadServiceImpl implements ReadService{

	@Autowired
	private UserDao userDao;
	
	@Override
	public List<UserStatsTO> readRanking() {
		
		return userDao.readRanking();
	}

	@Override
	public UserStatsTO getUserStats(long id) throws UserValidationException {
		
		UserStatsTO userStatsTo = userDao.findStatsById(id);
		if(userStatsTo == null){
			throw new UserValidationException("User does not exist");
		}
		return userStatsTo;
	}

}
