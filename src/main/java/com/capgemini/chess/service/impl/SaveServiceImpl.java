package com.capgemini.chess.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.chess.service.SaveService;
import com.capgemini.chess.service.access.MatchDao;
import com.capgemini.chess.service.to.MatchTO;
@Service
public class SaveServiceImpl implements SaveService{
	
	@Autowired
	private MatchDao matchDao;

	@Override
	public MatchTO saveMatch(MatchTO matchTo) {
		matchDao.saveMatch(matchTo);
		return matchTo;
	}
}
