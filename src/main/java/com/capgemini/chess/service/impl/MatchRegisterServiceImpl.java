package com.capgemini.chess.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.chess.exception.UserValidationException;
import com.capgemini.chess.service.MatchRegisterService;
import com.capgemini.chess.service.SaveService;
import com.capgemini.chess.service.StatisticsUpdateService;
import com.capgemini.chess.service.UserValidationService;
import com.capgemini.chess.service.to.MatchTO;

@Service
public class MatchRegisterServiceImpl implements MatchRegisterService{

	@Autowired
	private UserValidationService userValidationService;
	@Autowired
	private StatisticsUpdateService statisticsUpdateService;
	@Autowired
	private SaveService saveService;
	
	@Override
	public MatchTO registerNewMatch(MatchTO matchTo) throws UserValidationException {
		
		userValidationService.validate(matchTo.getGuestId());
		userValidationService.validate(matchTo.getHostId());
		statisticsUpdateService.updateStatistics(matchTo);
		saveService.saveMatch(matchTo);
		return matchTo;
	}

}
