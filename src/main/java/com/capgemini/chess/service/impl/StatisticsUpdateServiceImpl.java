package com.capgemini.chess.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.chess.enums.MatchResult;
import com.capgemini.chess.enums.PlayerResult;
import com.capgemini.chess.exception.UserValidationException;
import com.capgemini.chess.service.CalculateUserStatsService;
import com.capgemini.chess.service.ReadService;
import com.capgemini.chess.service.StatisticsUpdateService;
import com.capgemini.chess.service.access.UserDao;
import com.capgemini.chess.service.to.MatchTO;
import com.capgemini.chess.service.to.UserStatsTO;

@Service
public class StatisticsUpdateServiceImpl implements StatisticsUpdateService {

	@Autowired
	private ReadService readService;
	@Autowired
	private CalculateUserStatsService calculateStatsService;
	@Autowired 
	private UserDao userDao;

	@Override
	public MatchTO updateStatistics(MatchTO matchTo) throws UserValidationException {

		UserStatsTO hostStats = readService.getUserStats(matchTo.getHostId());
		UserStatsTO guestStats = readService.getUserStats(matchTo.getGuestId());
		UserStatsTO calculatedHostStats, calculatedGuestStats;
		
		if (matchTo.getResult() == MatchResult.HostWon) {
			calculatedHostStats = calculateStatsService.calculateStats(hostStats, PlayerResult.Won);
			calculatedGuestStats = calculateStatsService.calculateStats(guestStats, PlayerResult.Lost);
		} else if (matchTo.getResult() == MatchResult.GuestWon) {
			calculatedHostStats = calculateStatsService.calculateStats(hostStats, PlayerResult.Lost);
			calculatedGuestStats = calculateStatsService.calculateStats(guestStats, PlayerResult.Won);
		} else {
			calculatedHostStats = calculateStatsService.calculateStats(hostStats, PlayerResult.Draw);
			calculatedGuestStats = calculateStatsService.calculateStats(guestStats, PlayerResult.Draw);
		}
		userDao.updateUserStats(calculatedHostStats);
		userDao.updateUserStats(calculatedGuestStats);
		
		return matchTo;
	}	
}
