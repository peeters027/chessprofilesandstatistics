package com.capgemini.chess.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.chess.service.CreateRankingService;
import com.capgemini.chess.service.ReadService;
import com.capgemini.chess.service.to.RankingTO;
import com.capgemini.chess.service.to.UserStatsTO;

@Service
public class CreateRankingServiceImpl implements CreateRankingService {

	@Autowired
	private ReadService readService;

	@Override
	public RankingTO create(long id) {
		List<UserStatsTO> ranking = readService.readRanking();
		int rankingPosition = getRankingPosition(id, ranking);
		RankingTO rankingTO = new RankingTO();
		rankingTO.setRanking(ranking);
		rankingTO.setRankingPlace(rankingPosition);
		return rankingTO;
	}

	private int getRankingPosition(long id, List<UserStatsTO> ranking) {
		int rankingPosition = 0;
		for (int i = 0; i < ranking.size(); i++) {
			if (id == ranking.get(i).getId()) {
				rankingPosition = i + 1;
			}
		}
		return rankingPosition;
	}

}
