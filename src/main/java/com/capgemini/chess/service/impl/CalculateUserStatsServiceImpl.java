package com.capgemini.chess.service.impl;

import org.springframework.stereotype.Service;

import com.capgemini.chess.enums.Level;
import com.capgemini.chess.enums.PlayerResult;
import com.capgemini.chess.service.CalculateUserStatsService;
import com.capgemini.chess.service.to.UserStatsTO;

@Service
public class CalculateUserStatsServiceImpl implements CalculateUserStatsService {

	@Override
	public UserStatsTO calculateStats(UserStatsTO userStats, PlayerResult result) {

		int gamesPlayed = userStats.getGamesPlayed();
		int gamesWon = userStats.getWon();
		int gamesLost = userStats.getLost();
		int draws = userStats.getDraw();
		int rankingPoints = userStats.getRankingPoints();
		Level level;

		if (result == PlayerResult.Won) {
			rankingPoints = addPoints(rankingPoints);
			gamesWon++;
		}
		if (result == PlayerResult.Lost) {
			rankingPoints = subtractPoints(rankingPoints);
			gamesLost++;
		} else if (result == PlayerResult.Draw) {
			draws++;
		}
		gamesPlayed++;

		int levelValue = getLevelValue(gamesPlayed, gamesWon, rankingPoints);
		level = Level.getLevelByValue(levelValue);

		userStats.setGamesPlayed(gamesPlayed);
		userStats.setWon(gamesWon);
		userStats.setLost(gamesLost);
		userStats.setDraw(draws);
		userStats.setRankingPoints(rankingPoints);
		userStats.setLevel(level);
		return userStats;
	}

	private int addPoints(int rankingPoints) {
		rankingPoints = rankingPoints + 10;
		return rankingPoints;
	}

	private int subtractPoints(int rankingPoints) {
		if (rankingPoints > 0) {
			rankingPoints = rankingPoints - 10;
		}
		return rankingPoints;
	}

	private int getLevelValue(int gamesPlayed, int gamesWon, int rankingPoints) {
		int ratio = (gamesWon * 100) / gamesPlayed;

		if (rankingPoints >= 76801 && gamesPlayed >= 405 && ratio >= 72) {
			return 10;
		}
		if (rankingPoints >= 31401 && gamesPlayed >= 320 && ratio >= 64) {
			return 9;
		}
		if (rankingPoints >= 19201 && gamesPlayed >= 245 && ratio >= 56) {
			return 8;
		}
		if (rankingPoints >= 9601 && gamesPlayed >= 180 && ratio >= 48) {
			return 7;
		}
		if (rankingPoints >= 4801 && gamesPlayed >= 125 && ratio >= 40) {
			return 6;
		}
		if (rankingPoints >= 2401 && gamesPlayed >= 80 && ratio >= 32) {
			return 5;
		}
		if (rankingPoints >= 1201 && gamesPlayed >= 45 && ratio >= 24) {
			return 4;
		}
		if (rankingPoints >= 601 && gamesPlayed >= 20 && ratio >= 16) {
			return 3;
		}
		if (rankingPoints >= 301 && gamesPlayed >= 5 && ratio >= 8) {
			return 2;
		}
		return 1;

	}

}
