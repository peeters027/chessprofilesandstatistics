package com.capgemini.chess.service;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import com.capgemini.chess.dataaccess.dao.impl.MapUserDao;
import com.capgemini.chess.dataaccess.entities.UserEntity;
import com.capgemini.chess.enums.Level;
import com.capgemini.chess.service.mapper.UserProfileMapper;
import com.capgemini.chess.service.mapper.UserStatisticsMapper;
import com.capgemini.chess.service.to.UpdateProfileTO;
import com.capgemini.chess.service.to.UserProfileTO;
import com.capgemini.chess.service.to.UserStatsTO;


@RunWith(MockitoJUnitRunner.class)
public class UserDaoTest {

	@Before
	public void init() {
		setDataBase();
	}

	@After
	public void cleanDataBase() {
		Map<Long, UserEntity> users = new HashMap<>();
		users = MapUserDao.getUsers();
		users.clear();
	}
	
	@Test
	public void saveUserTest()
	{
		
		UserProfileTO fourthUser = new UserProfileTO.Builder()
				.login("chessLover")
				.email("rysio@gmail.com")
				.password("qwertyui")
				.name("Ryszard")
				.surname("Grabowski")
				.aboutMe("Jestem z miasta")
				.lifeMotto("")
				.build();
		
		MapUserDao userDao = new MapUserDao();
		userDao.save(fourthUser);
		UserEntity savedUser = MapUserDao.getUsers().get(fourthUser.getId());
		assertEquals(savedUser.getName(), fourthUser.getName());
		assertEquals(savedUser.getEmail(), fourthUser.getEmail());
		assertEquals(savedUser.getPassword(), fourthUser.getPassword());
	}
	
	@Test
	public void updateUserTest()
	{		
		UpdateProfileTO updateDataProfile = new UpdateProfileTO.Builder().id(1)
				.name("Marek")
				.email("marek@gmail.com")
				.password("321321321")
				.build();
		
		MapUserDao userDao = new MapUserDao();
		userDao.update(updateDataProfile);
		
		String entityName = MapUserDao.getUsers().get(updateDataProfile.getId()).getName();
		String entityEmail = MapUserDao.getUsers().get(updateDataProfile.getId()).getEmail();
		String entityPassword = MapUserDao.getUsers().get(updateDataProfile.getId()).getPassword();

		assertEquals(updateDataProfile.getName(), entityName);
		assertEquals(updateDataProfile.getEmail(), entityEmail);
		assertEquals(updateDataProfile.getPassword(), entityPassword);
		
	}
	
	@Test
	public void updateUserStats()
	{
		UserStatsTO userStatsTo = new UserStatsTO.Builder().id(1).gamesPlayed(10)
				.won(5)
				.lost(5)
				.draw(0)
				.rankingPoints(100)
				.level(Level.BEGINNER)
				.build();
		
		UserProfileTO userTo = new UserProfileTO.Builder().id(1)
				.login("peeters027")
				.name("Piotr")
				.email("piotr@gmail.com")
				.password("123123123")
				.statistics(userStatsTo)
				.build();
		
		UserStatsTO userUpdatedStats = new UserStatsTO.Builder().id(1)
				.gamesPlayed(11)
				.won(6)
				.lost(5)
				.draw(0)
				.rankingPoints(110)
				.level(Level.NEWBIE)
				.build();
		
		MapUserDao userDao = new MapUserDao();
		userDao.save(userTo);
		userDao.updateUserStats(userUpdatedStats);

		int gamesPlayed = MapUserDao.getUsers().get(userUpdatedStats.getId()).getStatistics().getGamesPlayed();
		int won = MapUserDao.getUsers().get(userUpdatedStats.getId()).getStatistics().getWon();
		int lost = MapUserDao.getUsers().get(userUpdatedStats.getId()).getStatistics().getLost();
		int draw = MapUserDao.getUsers().get(userUpdatedStats.getId()).getStatistics().getDraw();
		int rankingPoints = MapUserDao.getUsers().get(userUpdatedStats.getId()).getStatistics().getRankingPoints();
		Level level = MapUserDao.getUsers().get(userUpdatedStats.getId()).getStatistics().getLevel();

		assertEquals(userUpdatedStats.getGamesPlayed(), gamesPlayed);
		assertEquals(userUpdatedStats.getWon(), won);
		assertEquals(userUpdatedStats.getLost(), lost);
		assertEquals(userUpdatedStats.getDraw(), draw);
		assertEquals(userUpdatedStats.getRankingPoints(), rankingPoints);
		assertEquals(userUpdatedStats.getLevel(), level);
		
	}
	
	@Test
	public void sortingRankingTest()
	{	
		MapUserDao userDao = new MapUserDao();
		List<UserStatsTO> ranking = userDao.readRanking();

		long idOfUserWith1000p = 1;
		long idOfUserWith2000p = 2;
		long idOfUserWith500p = 3;

		assertEquals(ranking.get(0).getRankingPoints(),
				MapUserDao.getUsers().get(idOfUserWith2000p).getStatistics().getRankingPoints());
		assertEquals(ranking.get(1).getRankingPoints(),
				MapUserDao.getUsers().get(idOfUserWith1000p).getStatistics().getRankingPoints());
		assertEquals(ranking.get(2).getRankingPoints(),
				MapUserDao.getUsers().get(idOfUserWith500p).getStatistics().getRankingPoints());
	
	}
	
	public void setDataBase()
	{
		//MapUserDao userDao = new MapUserDao();
		
		UserStatsTO firstUserStats = new UserStatsTO.Builder().id(1).gamesPlayed(100)
				.won(50)
				.lost(50)
				.draw(0)
				.rankingPoints(1000)
				.level(Level.BEGINNER)
				.build();
		
		UserProfileTO firstUser = new UserProfileTO.Builder().id(1)
				.login("Lion")
				.email("piotr@gmail.com")
				.password("123123123")
				.name("Piotr")
				.surname("Szmolke")
				.aboutMe("Happy")
				.lifeMotto("Dopóki walczysz jesteś zwycięzcą")
				.statistics(firstUserStats)
				.build();
		
		UserStatsTO secondUserStats = new UserStatsTO.Builder().id(2).gamesPlayed(100)
				.won(50)
				.lost(50)
				.draw(0)
				.rankingPoints(2000)
				.level(Level.BEGINNER)
				.build();
		
		UserProfileTO secondUser = new UserProfileTO.Builder().id(2)
				.login("SpiderMan")
				.email("marek@gmail.com")
				.password("qazwsx3edc")
				.name("Marek")
				.surname("Nowak")
				.aboutMe("...")
				.lifeMotto("Always look on the bright side of life")
				.statistics(secondUserStats)
				.build();
		
		UserStatsTO thirdUserStats = new UserStatsTO.Builder().id(3).gamesPlayed(100)
				.won(50)
				.lost(50)
				.draw(0)
				.rankingPoints(500)
				.level(Level.BEGINNER)
				.build();
		
		UserProfileTO thirdUser = new UserProfileTO.Builder().id(3)
				.login("crazyGirl")
				.email("ania@gmail.com")
				.password("53423424432")
				.name("Anna")
				.surname("Kowalska")
				.aboutMe("I love chess!")
				.lifeMotto("Just do it!")
				.statistics(thirdUserStats)
				.build();
		
		// userDao.save(firstUser);
		// userDao.save(secondUser);
		// userDao.save(thirdUser);
		long firstId = 1;
		long secondId = 2;
		long thirdId = 3;

		UserEntity firstUserEntity = UserProfileMapper.map(firstUser);
		firstUserEntity.setStatistics(UserStatisticsMapper.map(firstUser.getStatistics()));

		UserEntity secondUserEntity = UserProfileMapper.map(secondUser);
		secondUserEntity.setStatistics(UserStatisticsMapper.map(secondUser.getStatistics()));

		UserEntity thirdUserEntity = UserProfileMapper.map(thirdUser);
		thirdUserEntity.setStatistics(UserStatisticsMapper.map(thirdUser.getStatistics()));

		MapUserDao.getUsers().put(firstId, firstUserEntity);
		MapUserDao.getUsers().put(secondId, secondUserEntity);
		MapUserDao.getUsers().put(thirdId, thirdUserEntity);
	}
}
