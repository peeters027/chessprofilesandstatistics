package com.capgemini.chess.service;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import com.capgemini.chess.exception.UserValidationException;
import com.capgemini.chess.service.access.UserDao;
import com.capgemini.chess.service.impl.UserValidationServiceImpl;
import com.capgemini.chess.service.to.UpdateProfileTO;
import com.capgemini.chess.service.to.UserProfileTO;

@RunWith(MockitoJUnitRunner.class)
public class UserValidationTests {

	@Mock UserDao userDao;
	
	@InjectMocks
	UserValidationServiceImpl userValidationServiceImpl;
	
	@org.junit.Test(expected=UserValidationException.class)
	public void shouldThrowExceptionWhenPasswordIsTooShort() throws UserValidationException {
		
		//given
		UserProfileTO userBeforeUpdate = new UserProfileTO.Builder().id(10)
														            .name("Piotr")
														            .password("123123123")
														            .email("piotr@gmail.com")
														            .build();
		
		UpdateProfileTO userWithNewUpdatedPassword = new UpdateProfileTO.Builder().id(10)
																				  .name("Piotr")
																				  .password("123qwe")
																				  .email("piotr@gmail.com")
																				  .build();
	
		when(userDao.findById(10)).thenReturn(userBeforeUpdate);
		when(userDao.findByEmail("piotr@gmail.com")).thenReturn(userBeforeUpdate);
		//when
		userValidationServiceImpl.validate(userWithNewUpdatedPassword);
		//then
		fail("This test should throw exception");
	}
		
		@org.junit.Test(expected=UserValidationException.class)
		public void shouldThrowExceptionWhenMailAlreadyTaken() throws UserValidationException {
		
			//given
			UserProfileTO someUserExistedInBase = new UserProfileTO.Builder().id(10)
		            .name("Piotr")
		            .password("123123123")
		            .email("piotr@gmail.com")
		            .build();
			
			UserProfileTO userBeforeUpdate = new UserProfileTO.Builder().id(11)
		            .name("Piotr")
		            .password("123123123")
		            .email("peterek@gmail.com")
		            .build();
			
			UpdateProfileTO userWithNewUpdatedEmail = new UpdateProfileTO.Builder().id(11)
					  .name("Piotr")
					  .password("123123123")
					  .email("piotr@gmail.com")
					  .build();
				
		when(userDao.findById(11)).thenReturn(userBeforeUpdate);
		when(userDao.findByEmail("piotr@gmail.com")).thenReturn(someUserExistedInBase);
		// when
		userValidationServiceImpl.validate(userWithNewUpdatedEmail);
		// then
		fail("This test should throw exception");
	}
		
		@org.junit.Test
		public void shouldPassValidationAfterUpdateCorrectPassword() throws UserValidationException {
			
			
			//given
			UserProfileTO userBeforeUpdate = new UserProfileTO.Builder().id(10)
															            .name("Piotr")
															            .password("123123123")
															            .email("piotr@gmail.com")
															            .build();
			
			UpdateProfileTO userWithNewUpdatedPassword = new UpdateProfileTO.Builder().id(10)
																					  .name("Piotr")
																					  .password("123qweasd")
																					  .email("piotr@gmail.com")
																					  .build();
		
			
			
		when(userDao.findById(10)).thenReturn(userBeforeUpdate);
		when(userDao.findByEmail("piotr@gmail.com")).thenReturn(userBeforeUpdate);
		boolean state = false;
		// when
		try {
			userValidationServiceImpl.validate(userWithNewUpdatedPassword);
		} catch (UserValidationException e) {
			state = true;
		}
		// then
		assertFalse(state);
	}
		
		@org.junit.Test(expected=UserValidationException.class)
		public void tryUpdateNonExistProfile() throws UserValidationException {
			
			
			UpdateProfileTO updatedUser = new UpdateProfileTO.Builder().id(10)
																		.name("Piotr")
																		.password("123qweasd")
																	    .email("piotr@gmail.com")
																	    .build();
		
			when(userDao.findById(10)).thenReturn(null);
			when(userDao.findByEmail("piotr@gmail.com")).thenReturn(null);
			userValidationServiceImpl.validate(updatedUser);
	
	}

}
