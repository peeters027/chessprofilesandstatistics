package com.capgemini.chess.service;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.capgemini.chess.enums.Level;
import com.capgemini.chess.enums.PlayerResult;
import com.capgemini.chess.service.impl.CalculateUserStatsServiceImpl;
import com.capgemini.chess.service.to.UserStatsTO;

public class CalculateUserStatsServiceTest {


	@Test
	public void calculatingStatsIfWon()
	{
		UserStatsTO inputStats = new UserStatsTO.Builder().id(1).gamesPlayed(100)
				.won(50)
				.lost(50)
				.draw(0)
				.rankingPoints(1000)
				.build();
		
		UserStatsTO expectedStatsIfWon = new UserStatsTO.Builder().id(1).gamesPlayed(101)
				.won(51)
				.lost(50)
				.draw(0)
				.rankingPoints(1010)
				.level(Level.BEGINNER)
				.build();	
		
		CalculateUserStatsServiceImpl calculate = new CalculateUserStatsServiceImpl();

		UserStatsTO resultIfWon = calculate.calculateStats(inputStats, PlayerResult.Won);

		// if won
		assertEquals(expectedStatsIfWon.getGamesPlayed(), resultIfWon.getGamesPlayed());
		assertEquals(expectedStatsIfWon.getWon(), resultIfWon.getWon());
		assertEquals(expectedStatsIfWon.getLost(), resultIfWon.getLost());
		assertEquals(expectedStatsIfWon.getDraw(), resultIfWon.getDraw());
		assertEquals(expectedStatsIfWon.getRankingPoints(), resultIfWon.getRankingPoints());
		assertEquals(expectedStatsIfWon.getLevel(), resultIfWon.getLevel());
		
	}
	
	@Test
	public void calculatingStatsIfLost()
	{
		UserStatsTO inputStats = new UserStatsTO.Builder().id(1).gamesPlayed(100)
				.won(50)
				.lost(50)
				.draw(0)
				.rankingPoints(1000)
				.build();
		
		UserStatsTO expectedStatsIfLost = new UserStatsTO.Builder().id(1).gamesPlayed(101)
				.won(50)
				.lost(51)
				.draw(0)
				.rankingPoints(990)
				.level(Level.BEGINNER)
				.build();
			
		CalculateUserStatsServiceImpl calculate = new CalculateUserStatsServiceImpl();
		UserStatsTO resultIfLost = calculate.calculateStats(inputStats, PlayerResult.Lost);

		// if lost
		assertEquals(expectedStatsIfLost.getGamesPlayed(), resultIfLost.getGamesPlayed());
		assertEquals(expectedStatsIfLost.getWon(), resultIfLost.getWon());
		assertEquals(expectedStatsIfLost.getLost(), resultIfLost.getLost());
		assertEquals(expectedStatsIfLost.getDraw(), resultIfLost.getDraw());
		assertEquals(expectedStatsIfLost.getRankingPoints(), resultIfLost.getRankingPoints());
		assertEquals(expectedStatsIfLost.getLevel(), resultIfLost.getLevel());
		
	}
	
	@Test
	public void calculatingStatsIfDraw()
	{
		UserStatsTO inputStats = new UserStatsTO.Builder().id(1).gamesPlayed(100)
				.won(50)
				.lost(50)
				.draw(0)
				.rankingPoints(1000)
				.build();
		
		UserStatsTO expectedStatsIfDraw = new UserStatsTO.Builder().id(1).gamesPlayed(101)
				.won(50)
				.lost(50)
				.draw(1)
				.rankingPoints(1000)
				.level(Level.BEGINNER)
				.build();
		
		
		CalculateUserStatsServiceImpl calculate = new CalculateUserStatsServiceImpl();
		UserStatsTO resultIfDraw = calculate.calculateStats(inputStats, PlayerResult.Draw);

		// if draw
		assertEquals(expectedStatsIfDraw.getGamesPlayed(), resultIfDraw.getGamesPlayed());
		assertEquals(expectedStatsIfDraw.getWon(), resultIfDraw.getWon());
		assertEquals(expectedStatsIfDraw.getLost(), resultIfDraw.getLost());
		assertEquals(expectedStatsIfDraw.getDraw(), resultIfDraw.getDraw());
		assertEquals(expectedStatsIfDraw.getRankingPoints(), resultIfDraw.getRankingPoints());
		assertEquals(expectedStatsIfDraw.getLevel(), resultIfDraw.getLevel());
		
	}

	
	
}
