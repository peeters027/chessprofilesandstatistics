package com.capgemini.chess.service;

import static org.mockito.Mockito.when;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import com.capgemini.chess.exception.UserValidationException;
import com.capgemini.chess.service.access.UserDao;
import com.capgemini.chess.service.impl.ReadServiceImpl;

@RunWith(MockitoJUnitRunner.class)

public class ReadServiceTest {

	@Mock
	UserDao userDao;

	@InjectMocks
	ReadServiceImpl readService;

	@org.junit.Test(expected = UserValidationException.class)
	public void shouldThrowExceptionWhenFoundAnyStats() throws UserValidationException {

		when(userDao.findStatsById(10)).thenReturn(null);

		readService.getUserStats(10);

	}

}
