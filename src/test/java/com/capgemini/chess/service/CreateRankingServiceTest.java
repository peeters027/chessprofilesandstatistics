package com.capgemini.chess.service;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import com.capgemini.chess.enums.Level;
import com.capgemini.chess.service.impl.CreateRankingServiceImpl;
import com.capgemini.chess.service.to.RankingTO;
import com.capgemini.chess.service.to.UserStatsTO;

@RunWith(MockitoJUnitRunner.class)
public class CreateRankingServiceTest {

	@Mock
	ReadService readService;

	@InjectMocks
	CreateRankingServiceImpl createRankingServiceImpl;

	@org.junit.Test
	public void creatingPositionInWholeRankingBasedOnId() {
		
		UserStatsTO userStatsTo1000p = new UserStatsTO.Builder().id(1).gamesPlayed(100)
				.won(50)
				.lost(50)
				.draw(0)
				.rankingPoints(1000)
				.level(Level.BEGINNER)
				.build();
		
		UserStatsTO userStatsTo2000p = new UserStatsTO.Builder().id(2).gamesPlayed(100)
				.won(50)
				.lost(50)
				.draw(0)
				.rankingPoints(2000)
				.level(Level.BEGINNER)
				.build();
		
		UserStatsTO userStatsTo500p = new UserStatsTO.Builder().id(3).gamesPlayed(100)
				.won(50)
				.lost(50)
				.draw(0)
				.rankingPoints(500)
				.level(Level.BEGINNER)
				.build();
		
		UserStatsTO userStatsTo5000p = new UserStatsTO.Builder().id(4).gamesPlayed(100)
				.won(50)
				.lost(50)
				.draw(0)
				.rankingPoints(5000)
				.level(Level.BEGINNER)
				.build();
		
		List<UserStatsTO> ranking = new ArrayList<UserStatsTO>();
		ranking.add(userStatsTo5000p);
		ranking.add(userStatsTo2000p);
		ranking.add(userStatsTo1000p);
		ranking.add(userStatsTo500p);

		when(readService.readRanking()).thenReturn(ranking);
		RankingTO rankingTo = createRankingServiceImpl.create(2);
		assertEquals(rankingTo.getRankingPlace(), 2);
		
	}
}
