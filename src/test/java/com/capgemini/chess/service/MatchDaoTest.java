package com.capgemini.chess.service;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.capgemini.chess.dataaccess.dao.impl.MapMatchDao;
import com.capgemini.chess.dataaccess.entities.MatchEntity;
import com.capgemini.chess.enums.MatchResult;
import com.capgemini.chess.service.to.MatchTO;

@RunWith(MockitoJUnitRunner.class)
public class MatchDaoTest {

	@Test
	public void saveMatchTest() {
		MatchTO matchTo = new MatchTO();
		matchTo.setHostId(10);
		matchTo.setGuestId(13);
		matchTo.setResult(MatchResult.HostWon);

		MapMatchDao matchDao = new MapMatchDao();
		matchDao.saveMatch(matchTo);

		MatchEntity savedMatch = MapMatchDao.getMatches().get(matchTo.getId());

		assertEquals(savedMatch.getHostId(), matchTo.getHostId());
		assertEquals(savedMatch.getGuestId(), matchTo.getGuestId());
		assertEquals(savedMatch.getMatchResult(), matchTo.getResult());

	}
}
