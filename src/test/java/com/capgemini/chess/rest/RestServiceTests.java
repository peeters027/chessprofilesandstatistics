package com.capgemini.chess.rest;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.capgemini.chess.enums.Level;
import com.capgemini.chess.enums.MatchResult;
import com.capgemini.chess.service.MatchRegisterService;
import com.capgemini.chess.service.RankingService;
import com.capgemini.chess.service.UserUpdateService;
import com.capgemini.chess.service.to.MatchTO;
import com.capgemini.chess.service.to.RankingTO;
import com.capgemini.chess.service.to.UpdateProfileTO;
import com.capgemini.chess.service.to.UserProfileTO;
import com.capgemini.chess.service.to.UserStatsTO;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(MockitoJUnitRunner.class)
public class RestServiceTests {

	@Mock
	private RankingService rankingService;

	@Mock
	private UserUpdateService userUpdateService;

	@Mock
	private MatchRegisterService matchRegisterService;

	@InjectMocks
	private RestService restService;

	private MockMvc mockMvc;

	@Before
	public void setup() {
		Mockito.reset(rankingService);
		Mockito.reset(userUpdateService);
		this.mockMvc = MockMvcBuilders.standaloneSetup(restService).build();
	}

	@Test
	public void shouldGetUserRanking() throws Exception {

		// given:
		final RankingTO rankingTo1 = new RankingTO();
		rankingTo1.setRankingPlace(2);
		rankingTo1.setRanking(setRanking());
		Mockito.when(rankingService.getRanking(1)).thenReturn(rankingTo1);

		// when
		ResultActions response = this.mockMvc.perform(get("/findStats/1"));

		// then
		response.andExpect(status().isOk())
				.andExpect(jsonPath("ranking[0].id").value((int) rankingTo1.getRanking().get(0).getId()))
				.andExpect(jsonPath("ranking[1].id").value((int) rankingTo1.getRanking().get(1).getId()))
				.andExpect(jsonPath("ranking[2].id").value((int) rankingTo1.getRanking().get(2).getId()))
				.andExpect(jsonPath("rankingPlace").value(rankingTo1.getRankingPlace()));
	}

	@Test(expected = Exception.class)
	public void shouldThrowExceptionDuringGetUserRanking() throws Exception {
		// given
		Mockito.when(rankingService.getRanking(1L)).thenThrow(new Exception("User with given ID does not exsist!"));

		// when
		ResultActions response = this.mockMvc.perform(get("/findStats/1"));

		// then
		assertEquals(response, new Exception());
		Mockito.verify(rankingService).getRanking(1L);
	}

	@Test
	public void shouldUpdateUserProfile() throws Exception {

		// given:
		UpdateProfileTO updateUser = setUpdateProfile();
		UserProfileTO userAfterUpdate = setExpectedProfileAfterUpdate();
		String jsonUpdateUser = new ObjectMapper().writeValueAsString(updateUser);
		Mockito.when(userUpdateService.updateProfile(Mockito.any())).thenReturn(userAfterUpdate);

		// when
		ResultActions response = this.mockMvc.perform(post("/updateUser").accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON).content(jsonUpdateUser));

		// then
		response.andExpect(status().isOk()).andExpect(jsonPath("id").value((int) userAfterUpdate.getId()))
				.andExpect(jsonPath("name").value(userAfterUpdate.getName()))
				.andExpect(jsonPath("surname").value(userAfterUpdate.getSurname()))
				.andExpect(jsonPath("lifeMotto").value(userAfterUpdate.getLifeMotto()));
	}

	@Test(expected = AssertionError.class)
	public void shouldThrowExceptionDuringUpdatingUserProfile() throws Exception {

		// given
		UpdateProfileTO update = new UpdateProfileTO();
		String jsonUpdate = new ObjectMapper().writeValueAsString(update);

		// when
		Mockito.when(userUpdateService.updateProfile(update)).thenReturn(null);

		// then
		ResultActions response = this.mockMvc.perform(post("/updateUser").accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON).content(jsonUpdate));

		assertEquals(response, new Exception());
		Mockito.verify(userUpdateService).updateProfile(update);
	}

	@Test
	public void shouldRegisterNewMatch() throws Exception {

		// given:
		MatchTO matchTo = new MatchTO();
		matchTo.setHostId(1);
		matchTo.setGuestId(2);
		matchTo.setResult(MatchResult.HostWon);

		MatchTO savedMatchTo = new MatchTO();
		savedMatchTo.setId(1L);
		savedMatchTo.setHostId(1);
		savedMatchTo.setGuestId(2);
		savedMatchTo.setResult(MatchResult.HostWon);

		String jsonMatchTo = new ObjectMapper().writeValueAsString(matchTo);
		Mockito.when(matchRegisterService.registerNewMatch(Mockito.any())).thenReturn(savedMatchTo);

		// when
		ResultActions response = this.mockMvc.perform(post("/registerNewMatch").accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON).content(jsonMatchTo));

		// then
		response.andExpect(status().isOk()).andExpect(jsonPath("id").value((int) savedMatchTo.getId()))
				.andExpect(jsonPath("hostId").value((int) savedMatchTo.getHostId()))
				.andExpect(jsonPath("guestId").value((int) savedMatchTo.getGuestId()))
				.andExpect(jsonPath("result").value(savedMatchTo.getResult().toString()));
	}

	@Test(expected = AssertionError.class)
	public void shouldThrowExceptionDuringRegisteringNewMatch() throws Exception {

		// given
		MatchTO matchTo = new MatchTO();
		String jsonMatchTo = new ObjectMapper().writeValueAsString(matchTo);
		Mockito.when(matchRegisterService.registerNewMatch(matchTo)).thenReturn(null);

		// when
		ResultActions response = this.mockMvc.perform(post("/registerNewMatch").accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON).content(jsonMatchTo));

		// then
		assertEquals(response, new Exception());
		Mockito.verify(matchRegisterService).registerNewMatch(matchTo);
	}

	public List<UserStatsTO> setRanking() {

		List<UserStatsTO> userStats = new ArrayList<UserStatsTO>();

		UserStatsTO firstUserStats = new UserStatsTO.Builder().id(1).gamesPlayed(100).won(50).lost(50).draw(0)
				.rankingPoints(1000).level(Level.BEGINNER).build();

		UserStatsTO secondUserStats = new UserStatsTO.Builder().id(2).gamesPlayed(100).won(50).lost(50).draw(0)
				.rankingPoints(2000).level(Level.BEGINNER).build();

		UserStatsTO thirdUserStats = new UserStatsTO.Builder().id(3).gamesPlayed(100).won(50).lost(50).draw(0)
				.rankingPoints(500).level(Level.BEGINNER).build();

		userStats.add(secondUserStats);
		userStats.add(firstUserStats);
		userStats.add(thirdUserStats);
		return userStats;
	}

	public UpdateProfileTO setUpdateProfile() {
		UpdateProfileTO updateProfile = new UpdateProfileTO.Builder().id(1).name("Tomek").surname("Szmolke")
				.email("piotr@gmail.com").aboutMe("Happy").lifeMotto("Dopóki walczysz jesteś zwyciezcą").build();
		return updateProfile;
	}

	public UserProfileTO setExpectedProfileAfterUpdate() {
		UserStatsTO userStats = new UserStatsTO.Builder().id(1).gamesPlayed(100).won(50).lost(50).draw(0)
				.rankingPoints(1000).level(Level.BEGINNER).build();

		UserProfileTO expectedUserAfterUpdated = new UserProfileTO.Builder().id(1).login("Lion").name("Tomek")
				.surname("Szmolke").email("piotr@gmail.com").aboutMe("Happy")
				.lifeMotto("Dopóki walczysz jesteś zwyciezcą").statistics(userStats).build();
		return expectedUserAfterUpdated;
	}
}
